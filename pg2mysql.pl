#!/usr/bin/perl -Tw
use strict;

print "set time_zone = 'Australia/Sydney';\n";

my ($set, $table, $table_count, $index, $comment) = ('', '', 0, 0, 0);
my @input = (<>);
for my $i (@input) {

	chomp $i;

	$comment = ($i =~ /(^--|^$)/);

	# convert currval() to @
	$i =~ s/currval\('([a-zA-Z_]+)_seq'\)/\@$1/g;

	# convert timestamp
	$i =~ s/interval '([0-9]+) (year|years|month|months|weeks|week|hour|hours|day|days|minute|minutes)'/interval $1 $2/g;
	$i =~ s/interval ([0-9]+) (year|month|week|day|hour|minute)s/interval $1 $2/g;

	# convert function
	$i =~ s/date_trunc\('minute',\s*current_timestamp(\s+at\s+time\s+zone\s+'[^']+')?\)/date_format(current_timestamp$1, "%Y-%m-%d %H:%i:00")/g;

	# convert timezones
	$i =~ s/(current_timestamp|'[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01]?[0-9]):[0-5][0-9]:[0-5][0-9]')\s+at\s+time\s+zone\s+('[^']+')/convert_tz($1, \@\@session.time_zone, $5)/g;

	# escape global identifiers in columns
	$i =~ s/ (interval)(,|\))/ `$1`$2/g;

	# print line
	print $i, "\n";

	if (!$comment) {
		$set = 'SET @'.$1.'_id = LAST_INSERT_ID();'."\n" if ($i =~ /^INSERT INTO ([a-zA-Z_]+)/);
		print $set if defined $1 && $table ne $1 && !$table_count && $input[$index+1] !~ /^INSERT INTO $1 / && $input[$index-1] !~ /^INSERT INTO $1 /;
		$table = defined $1 ? $1 : '';
	}

	$index++;
}
